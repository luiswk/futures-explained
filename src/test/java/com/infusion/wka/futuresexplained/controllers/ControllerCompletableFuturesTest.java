package com.infusion.wka.futuresexplained.controllers;

import com.infusion.wka.futuresexplained.domain.Order;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.Result;
import com.infusion.wka.futuresexplained.domain.User;
import com.infusion.wka.futuresexplained.services.OrderService;
import com.infusion.wka.futuresexplained.services.PermissionService;
import com.infusion.wka.futuresexplained.services.UserService;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

/**
 * @author Wojciech Kaczmarek
 */
public class ControllerCompletableFuturesTest {

    private final UserService userService = UserService.createUserService();
    private final PermissionService permissionService = PermissionService.createPermissionService();
    private final OrderService orderService = OrderService.createOrderService();

    /**
     * - first compose()
     * - then combine() to a Result
     * - accept() at the end instead of a Callback
     */
    @Test
    public void should_wka_have_orders_permission_completable_futures() throws Exception {

    }

}
