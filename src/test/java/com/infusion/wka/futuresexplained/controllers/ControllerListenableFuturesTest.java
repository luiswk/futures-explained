package com.infusion.wka.futuresexplained.controllers;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.infusion.wka.futuresexplained.domain.Order;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.Result;
import com.infusion.wka.futuresexplained.domain.User;
import com.infusion.wka.futuresexplained.services.OrderService;
import com.infusion.wka.futuresexplained.services.PermissionService;
import com.infusion.wka.futuresexplained.services.UserService;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.List;
import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Wojciech Kaczmarek
 */
public class ControllerListenableFuturesTest {

    private final UserService userService = UserService.createUserService();
    private final PermissionService permissionService = PermissionService.createPermissionService();
    private final OrderService orderService = OrderService.createOrderService();

    private User user;
    private Permissions permissions;
    private Order order;

    /**
     * - same approach as with Future
     * - everything we have learnt with Futures applies here
     * - if we change operation order it runs faster
     */
    @Test
    public void should_wka_have_orders_permission_callbacks() throws Exception {
        ListenableFuture<User> fUser = userService.findByNameListenableFuture("wka");
        Futures.addCallback(fUser, new FutureCallback<User>() {
            @Override
            public void onSuccess(@Nullable User u) {
                ListenableFuture<Permissions> fPermissions = permissionService.findUserPermissionsListenableFuture(u);
                Futures.addCallback(fPermissions, new FutureCallback<Permissions>() {
                    @Override
                    public void onSuccess(@Nullable Permissions result) {

                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }

    /**
     * - not even a single thread blocking
     * - transformAsync allows to chain async operations
     * - allAsList creates ListenableFuture representing whole operation
     * - use fromList to transform List of Objects
     * - remember to sum up after implementation
     * - optional: run the test and comment on execution time
     */
    @Test
    public void should_wka_have_orders_permission_transform() throws Exception {
        ListenableFuture<User> fUser = userService.findByNameListenableFuture("wka");
        ListenableFuture<Permissions> fPermissions = Futures.transformAsync(fUser, u -> permissionService.findUserPermissionsListenableFuture(u));

        ListenableFuture<Order> fOrder = orderService.findOrderListenableFuture("my-order");

        ListenableFuture<List<Object>> fWholeOperation = Futures.allAsList(fPermissions, fOrder);
        ListenableFuture<Result> fResult = Futures.transform(fWholeOperation, this::fromList);
        Futures.addCallback(fResult, new FutureCallback<Result>() {
            @Override
            public void onSuccess(@Nullable Result result) {
                result.getOrder();
                result.getPermissions();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }

    private Result fromList(List<Object> obj) {
        return new Result((Permissions) obj.get(0), (Order) obj.get(1));
    }

}
