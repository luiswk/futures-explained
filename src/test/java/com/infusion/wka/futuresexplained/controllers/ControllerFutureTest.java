package com.infusion.wka.futuresexplained.controllers;

import com.infusion.wka.futuresexplained.domain.Order;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.User;
import com.infusion.wka.futuresexplained.services.OrderService;
import com.infusion.wka.futuresexplained.services.PermissionService;
import com.infusion.wka.futuresexplained.services.UserService;
import org.junit.Test;

import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Wojciech Kaczmarek
 */
public class ControllerFutureTest {

    private final UserService userService = UserService.createUserService();
    private final PermissionService permissionService = PermissionService.createPermissionService();
    private final OrderService orderService = OrderService.createOrderService();

    private User user;
    private Permissions permissions;
    private Order order;

    @Test
    public void should_wka_have_orders_permission_sync() throws Exception {
        user = userService.findByName("wka");
        permissions = permissionService.findUserPermissions(user);
        order = orderService.findOrder("my-order");

        assertThat(this.user, is(notNullValue()));
        assertThat(this.permissions.hasPermission("orders"), equalTo(true));
        assertThat(this.order, is(notNullValue()));
    }

    /**
     * - threads block on .get()
     * - moving async service calls to the top makes the test pass
     * - that's all we can get out of Future
     */
    @Test(timeout = 1200)
    public void should_wka_have_orders_permission_async() throws Exception {
        Future<User> fUser = userService.findByNameAsync("wka");
        user = fUser.get();

        Future<Permissions> fPermissions = permissionService.findUserPermissionsAsync(user);
        permissions = fPermissions.get();

        Future<Order> fOrder = orderService.findOrderAsync("my-order");
        order = fOrder.get();

        assertThat(user, is(notNullValue()));
        assertThat(permissions.hasPermission("orders"), equalTo(true));
        assertThat(order, is(notNullValue()));
    }

}
