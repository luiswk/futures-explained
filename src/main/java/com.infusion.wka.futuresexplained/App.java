package com.infusion.wka.futuresexplained;

import com.infusion.wka.futuresexplained.controllers.AsyncController;
import com.infusion.wka.futuresexplained.controllers.SyncController;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;

/**
 * @author Wojciech Kaczmarek
 */
public class App extends Application<Configuration> {

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public void run(Configuration config, Environment environment) throws Exception {
        environment.jersey().register(new AsyncController());
        environment.jersey().register(new SyncController());
    }

}
