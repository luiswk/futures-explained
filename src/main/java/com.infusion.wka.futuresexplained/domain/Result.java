package com.infusion.wka.futuresexplained.domain;

/**
 * @author Wojciech Kaczmarek
 */
public class Result {

    private Permissions permissions;
    private Order order;

    public Result(Permissions permissions, Order order) {
        this.permissions = permissions;
        this.order = order;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public Order getOrder() {
        return order;
    }
}
