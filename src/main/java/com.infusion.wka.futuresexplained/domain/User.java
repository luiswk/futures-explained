package com.infusion.wka.futuresexplained.domain;

/**
 * @author Wojciech Kaczmarek
 */
public class User {

    private String username;
    private String firstName;
    private String surname;

    public User(String username) {
        this.username = username;
    }

    public User(String username, String firstName, String surname) {
        this.username = username;
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }
}
