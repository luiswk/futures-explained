package com.infusion.wka.futuresexplained.controllers;

import com.infusion.wka.futuresexplained.domain.Order;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.Result;
import com.infusion.wka.futuresexplained.domain.User;
import com.infusion.wka.futuresexplained.services.OrderService;
import com.infusion.wka.futuresexplained.services.PermissionService;
import com.infusion.wka.futuresexplained.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import java.util.concurrent.CompletableFuture;

/**
 * @author Wojciech Kaczmarek
 */
@Path("async")
public class AsyncController {

    private static final Logger LOG = LoggerFactory.getLogger(AsyncController.class);

    private final UserService userService = UserService.createUserService();
    private final PermissionService permissionService = PermissionService.createPermissionService();
    private final OrderService orderService = OrderService.createOrderService();

    @GET
    @Path("user/{username}")
    public void helloAsync(@Suspended AsyncResponse response,
                           @PathParam("username") String username) {
        userService.findByNameCompletableFuture(username)
                .thenAccept(u -> response.resume(u.getFirstName()));
    }

    @GET
    @Path("check-order/{username}/{permission}/{order}")
    public void checkOrderPermission(@Suspended AsyncResponse response,
                                     @PathParam("username") String username,
                                     @PathParam("permission") String permission,
                                     @PathParam("order") String order) {
        CompletableFuture<User> cUser = userService.findByNameCompletableFuture(username);
        CompletableFuture<Permissions> cPermissions = cUser.thenCompose(permissionService::findUserPermissionsCompletableFuture);

        CompletableFuture<Order> cOrder = orderService.findOrderCompletableFuture(order);

        CompletableFuture<Result> fWholeOperation = cOrder.thenCombine(cPermissions, (ord, per) -> new Result(per, ord));
        fWholeOperation.thenAccept((res) ->
                response.resume(res.getOrder() != null && res.getPermissions().hasPermission(permission)));
    }
}
