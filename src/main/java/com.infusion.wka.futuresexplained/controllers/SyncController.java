package com.infusion.wka.futuresexplained.controllers;

import com.infusion.wka.futuresexplained.domain.Order;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.User;
import com.infusion.wka.futuresexplained.services.OrderService;
import com.infusion.wka.futuresexplained.services.PermissionService;
import com.infusion.wka.futuresexplained.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Wojciech Kaczmarek
 */
@Path("sync")
public class SyncController {

    private static final Logger LOG = LoggerFactory.getLogger(SyncController.class);

    private final UserService userService = UserService.createUserService();
    private final PermissionService permissionService = PermissionService.createPermissionService();
    private final OrderService orderService = OrderService.createOrderService();

    @GET
    @Path("user/{username}")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@PathParam("username") String username) {
        User user = userService.findByName(username);
        return user.getFirstName();
    }

    @GET
    @Path("check-order/{username}/{permission}/{order}")
    public boolean checkOrderPermission(@PathParam("username") String username,
                                        @PathParam("permission") String permission,
                                        @PathParam("order") String order) {
        // 500ms
        User user = userService.findByName(username);
        // 500ms
        Permissions permissions = permissionService.findUserPermissions(user);
        // 500ms
        Order theOrder = orderService.findOrder(order);
        return theOrder != null && permissions.hasPermission(permission);
    }

}
