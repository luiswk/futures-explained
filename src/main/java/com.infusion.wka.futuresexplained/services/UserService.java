package com.infusion.wka.futuresexplained.services;

import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.*;
import com.infusion.wka.futuresexplained.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author chbatey
 */
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1,
            new ThreadFactoryBuilder().setNameFormat("user-service-%d").build());
    private final ListeningScheduledExecutorService ls = MoreExecutors.listeningDecorator(executor);

    private final Map<String, User> users;

    private UserService(Map<String, User> users) {
        this.users = users;
    }

    public static UserService createUserService() {
        return new UserService(ImmutableMap.of(
                "wka", new User("wka", "Wojtek", "Kaczmarek")
        ));
    }

    public User findByName(String username) {
        Uninterruptibles.sleepUninterruptibly(Config.DELAY, TimeUnit.MILLISECONDS);
        LOG.info("User lookup complete");
        return users.get(username);
    }

    public Future<User> findByNameAsync(String username) {
        return executor.schedule(() -> {
            LOG.info("User lookup complete");
            return users.get(username);
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public ListenableFuture<User> findByNameListenableFuture(String username) {
        return ls.schedule(() -> {
            LOG.info("User lookup complete");
            return users.get(username);
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public CompletableFuture<User> findByNameCompletableFuture(String username) {
        CompletableFuture<User> result = new CompletableFuture<>();
        executor.schedule(() -> {
            LOG.info("User lookup complete");
            return result.complete(users.get(username));
        }, Config.DELAY, TimeUnit.MILLISECONDS);
        return result;
    }
}
