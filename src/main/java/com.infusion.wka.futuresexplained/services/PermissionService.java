package com.infusion.wka.futuresexplained.services;

import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.*;
import com.infusion.wka.futuresexplained.controllers.AsyncController;
import com.infusion.wka.futuresexplained.domain.Permissions;
import com.infusion.wka.futuresexplained.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author chbatey
 */
public class PermissionService {

    private static final Logger LOG = LoggerFactory.getLogger(PermissionService.class);

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1,
            new ThreadFactoryBuilder().setNameFormat("permission-service-%d").build());
    private final ListeningScheduledExecutorService ls = MoreExecutors.listeningDecorator(executor);

    private final Map<String, Permissions> permissions;

    private PermissionService(Map<String, Permissions> permissions) {
        this.permissions = permissions;
    }

    public static PermissionService createPermissionService() {
        return new PermissionService(ImmutableMap.of(
                "orders", new Permissions()
        ));
    }

    public Permissions findUserPermissions(User user) {
        Uninterruptibles.sleepUninterruptibly(Config.DELAY, TimeUnit.MILLISECONDS);
        LOG.info("Permission lookup complete");
        return permissions.get("orders");
    }

    public Future<Permissions> findUserPermissionsAsync(User user) {
        return executor.schedule(() -> {
            LOG.info("Permission lookup complete");
            return permissions.get("orders");
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public ListenableFuture<Permissions> findUserPermissionsListenableFuture(User user) {
        return ls.schedule(() -> {
            LOG.info("Permission lookup complete");
            return permissions.get("orders");
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public CompletableFuture<Permissions> findUserPermissionsCompletableFuture(User user) {
        CompletableFuture<Permissions> result = new CompletableFuture<>();
        executor.schedule(() -> {
            LOG.info("Permission lookup complete");
            return result.complete(permissions.get("orders"));
        }, Config.DELAY, TimeUnit.MILLISECONDS);
        return result;
    }

}
