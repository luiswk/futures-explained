package com.infusion.wka.futuresexplained.services;

import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.*;
import com.infusion.wka.futuresexplained.domain.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author chbatey
 */
public class OrderService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1,
            new ThreadFactoryBuilder().setNameFormat("order-service-%d").build());
    private final ListeningScheduledExecutorService ls = MoreExecutors.listeningDecorator(executor);

    private final Map<String, Order> orders;

    private OrderService(Map<String, Order> orders) {
        this.orders = orders;
    }

    public static OrderService createOrderService() {
        return new OrderService(ImmutableMap.of(
                "my-order", new Order()
        ));
    }

    public Order findOrder(String order) {
        Uninterruptibles.sleepUninterruptibly(Config.DELAY, TimeUnit.MILLISECONDS);
        LOG.info("Order lookup complete");
        return orders.get(order);
    }

    public Future<Order> findOrderAsync(String order) {
        return executor.schedule(() -> {
            LOG.info("Order lookup complete");
            return orders.get(order);
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public ListenableFuture<Order> findOrderListenableFuture(String order) {
        return ls.schedule(() -> {
            LOG.info("Order lookup complete");
            return orders.get(order);
        }, Config.DELAY, TimeUnit.MILLISECONDS);
    }

    public CompletableFuture<Order> findOrderCompletableFuture(String order) {
        CompletableFuture<Order> result = new CompletableFuture<>();
        executor.schedule(() -> {
            LOG.info("Order lookup complete");
            result.complete(orders.get(order));
        }, Config.DELAY, TimeUnit.MILLISECONDS);
        return result;
    }

}
