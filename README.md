# DISCLAIMER
The project's content was greatly inspired by the Christopher Batey's talk during the Devoxx Poland 2016 Java Conference.  
Some of the sources were actually copied from his project available at github.com/chbatey  
All credits go to Chris!

# Description
Project created to demonstrate different ways of implementing asynchronous endpoints using:  
- Future  
- ListenableFuture from Google's Guava  
- CompletableFuture from Java 8  

# Running the application server
Please add this as program parameters:  
server src/main/resources/config.yml